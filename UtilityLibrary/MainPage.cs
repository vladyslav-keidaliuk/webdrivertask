﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace UtilityLibrary
{
    public class MainPage
    {
        private readonly IWebDriver driver;

        private readonly By SignIn = By.CssSelector(
            "#gatsby-focus-wrapper > div.fixed.top-0.z-30.w-full.duration-75.ease-linear.bg-purple-25 > div > div > header > div > div.ml-auto.hidden.items-center.xl\\:shrink-0.md\\:flex.md\\:gap-4 > a.flex.flex-row.items-center.text-center.font-bold.rounded-full.group.transition-all.duration-300.no-underline.justify-center.items-center.transition-all.px-3.py-1\\.5.text-sm.text-purple-500.hover\\:\\!bg-purple-500.hover\\:\\!text-white.button-hover-shadow.focus\\:bg-purple-500.focus\\:text-white.w-fit");

        private readonly By UsernameEntry = By.CssSelector("#username");
        private readonly By PasswordEntry = By.CssSelector("#password");
        private readonly By ButtonSubmit = By.CssSelector("body > div.app-root  form > button");


        public MainPage(IWebDriver webDriver)
        {
            driver = webDriver;
        }

        public void LogIn(string EmailUsername, string Password)
        {
            try
            {
                driver.FindElement(SignIn).Click();
                Thread.Sleep(5000);

                var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
                Thread.Sleep(10000);

                var Entry = waiter.Until(w => w.FindElement(UsernameEntry));
                Entry.SendKeys(EmailUsername);

                Entry = waiter.Until(w => w.FindElement(PasswordEntry));
                Entry.SendKeys(Password);

                Entry = waiter.Until(w => w.FindElement(ButtonSubmit));
                Entry.Click();
                Thread.Sleep(8000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
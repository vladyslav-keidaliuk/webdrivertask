﻿using Microsoft.VisualBasic.CompilerServices;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace UtilityLibrary;

public class InboxPage
{
    private readonly IWebDriver driver;

    private readonly By MailString =
        By.CssSelector(".block.text-ellipsis.color-weak.text-sm.m-0.lh-rg.user-dropdown-email");

    private readonly By LastIncomingEmail =
        By.XPath(
            "/html/body/div[1]/div[3]/div/div[2]/div/div[2]/div/div/div/main/div/div/div/div/div/div[2]/div[2]/div[1]/div");

    private readonly By NewMessageButton =
        By.CssSelector("body > div.app-root  div.px-3.pb-2.flex-item-noshrink.no-mobile > button");

    private readonly By AddresseeField =
        By.XPath("/html/body/div[1]/div[4]/div/div/div/div/div/div[2]/div/div/div/div/div/div/input");

    private readonly By ThemeField = By.XPath("/html/body/div[1]/div[4]/div/div/div/div/div/div[3]/div/div/input");

    private readonly By MessageField = By.CssSelector("#rooster-editor");

    private readonly By SendButton = By.XPath("/html/body/div[1]/div[4]/div/div/div/footer/div/div[1]/button[1]");

    private readonly By IFrameEmailTextBox =
        By.XPath("/html/body/div[1]/div[4]/div/div/div/div/section/div/div[1]/div[1]/div/iframe");

    private readonly By UnreadEmails =
        By.XPath(
            "/html/body/div[1]/div[3]/div/div[2]/div/div[2]/div/div/div/main/div/div/div/div/div/div[1]/div/nav/div[2]/div[1]/div/button[3]");

    private readonly By SenderInMessage = By.CssSelector(
        "body > div.app-root span.flex-align-self-center.message-recipient-item-expanded-content > span > span > span > span > span.message-recipient-item-address.ml-1.color-primary");

    private readonly By TextInMessage = By.CssSelector("#proton-root > div > div > div:nth-child(1)");
    private readonly By IFrameTextInMessage = By.CssSelector("div > iframe");


    private readonly By Profile =
        By.XPath("/html/body/div[1]/div[3]/div/div[2]/div/div[2]/div/div/div/header/div[2]/ul/li[3]/button");

    private readonly By LogOutButton = By.XPath("/html/body/div[4]/div[2]/ul/li[5]/div/button");

    private readonly By ReplyButton =
        By.XPath(
            "/html/body/div[1]/div[3]/div/div[2]/div/div[2]/div/div/div/main/div/div/section/div/div[3]/div/div/div/article/div[1]/div[4]/div[2]/button[1]");


    private readonly By SettingsButton =
        By.XPath("/html/body/div[1]/div[3]/div/div[2]/div/div[2]/div/div/div/header/div[2]/ul/li[2]");

    private readonly By AllSettingsButton =
        By.CssSelector(
            "#drawer-app-proton-settings > div.flex-item-fluid.contacts-widget.w100 > div > div.scroll-inner > div > div > a");

    private readonly By AccountAndPassword =
        By.CssSelector(
            "body > div.app-root > div.flex.flex-row.flex-nowrap.h100 > div > div > div > div.sidebar.flex.flex-nowrap.flex-column.no-print.outline-none > div.flex-item-fluid.flex-nowrap.flex.flex-column.overflow-overlay.pb-4.md\\:mt-2 > nav > ul > ul:nth-child(1) > li:nth-child(5) > a");

    private readonly By EditButton =
        By.CssSelector(
            "#account > div:nth-child(1) > div:nth-child(2) > div.settings-layout-right.pt-2 > div > button");

    private readonly By InputForNickname = By.CssSelector("#displayName");

    private readonly By SaveButton =
        By.CssSelector("body > div.modal-two > dialog > form > div.modal-two-footer > button.button.button-solid-norm");

    private readonly By Nickname =
        By.CssSelector("#account > div:nth-child(1) > div:nth-child(2) > div.settings-layout-right.pt-2 > div > div");

    private readonly By ProfileInSettings =
        By.XPath("/html/body/div[1]/div[3]/div/div/div/div[2]/div/div/header/div[2]/ul/li[2]/button");

    private readonly By LogOutButtonInSettings =
        By.CssSelector("#dropdown-4 > div.dropdown-content > ul > li:nth-child(4) > div > button");

    public InboxPage(IWebDriver webDriver)
    {
        driver = webDriver;
    }

    public string GetEmailAdress()
    {
        try
        {
            var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(8));

            IWebElement element = waiter.Until(w => w.FindElement(MailString));
            string text = element.Text;
            return text;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return "None";
        }
    }

    public void LogOut()
    {
        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
        var element = wait.Until(d => d.FindElement(Profile));
        element.Click();

        element = wait.Until(d => d.FindElement(LogOutButton));
        element.Click();
    }

    public void LogOutFromSettings()
    {
        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));
        var element = wait.Until(d => d.FindElement(ProfileInSettings));
        Thread.Sleep(2000);
        element.Click();

        element = wait.Until(d => d.FindElement(LogOutButtonInSettings));
        element.Click();
    }

    public bool SendEmail(string Addressee, string Theme, string Text)
    {
        try
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(8));

            var element = wait.Until(d => d.FindElement(NewMessageButton));
            element.Click();

            element = wait.Until(d => d.FindElement(AddresseeField));
            element.SendKeys(Addressee);

            element = wait.Until(d => d.FindElement(ThemeField));
            element.SendKeys(Theme);

            element = driver.FindElement(IFrameEmailTextBox);

            driver.SwitchTo().Frame(element);

            element = driver.FindElement(MessageField);
            element.Clear();

            element.SendKeys(Text);

            driver.SwitchTo().DefaultContent();

            element = wait.Until(d => d.FindElement(SendButton));
            element.Click();

            Thread.Sleep(30000);

            return true;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return false;
        }
    }

    public bool OpenUnreadMessage()
    {
        try
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

            var element = wait.Until(w => w.FindElement(UnreadEmails));
            element.Click();

            Thread.Sleep(5000);
            element = wait.Until(w => w.FindElement(LastIncomingEmail));
            element.Click();
            Thread.Sleep(1000);

            return true;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return false;
        }
    }

    public string GetSender()
    {
        try
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            var element = wait.Until(w => w.FindElement(SenderInMessage));
            string sender = element.Text;

            Thread.Sleep(1000);

            return sender;
        }
        catch (Exception e)
        {
            return "None";
        }
    }

    public string GetText()
    {
        try
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            var element = wait.Until(w => w.FindElement(IFrameTextInMessage));
            driver.SwitchTo().Frame(element);

            Thread.Sleep(5000);

            element = wait.Until(w => w.FindElement(TextInMessage));
            string text = element.Text;

            driver.SwitchTo().DefaultContent();

            Thread.Sleep(1000);


            return text;
        }
        catch (Exception e)
        {
            return "None";
        }
    }

    public bool SendReply()
    {
        try
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            var element = wait.Until(w => w.FindElement(ReplyButton));
            element.Click();


            element = driver.FindElement(IFrameEmailTextBox);

            driver.SwitchTo().Frame(element);

            element = driver.FindElement(MessageField);
            element.Clear();
            element.SendKeys(TextGenerator.GenerateRandomText(10));

            driver.SwitchTo().DefaultContent();

            element = wait.Until(d => d.FindElement(SendButton));
            element.Click();

            return true;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return false;
        }
    }


    public void ChangeNickname(string nickname)
    {
        try
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            var element = wait.Until(d => d.FindElement(SettingsButton));
            element.Click();
            Thread.Sleep(1000);

            element = wait.Until(d => d.FindElement(AllSettingsButton));
            element.Click();
            Thread.Sleep(1000);

            element = wait.Until(d => d.FindElement(AccountAndPassword));
            element.Click();
            Thread.Sleep(1000);

            element = wait.Until(d => d.FindElement(EditButton));
            element.Click();
            Thread.Sleep(1000);

            element = wait.Until(d => d.FindElement(InputForNickname));
            element.SendKeys(nickname);
            Thread.Sleep(1000);

            element = wait.Until(d => d.FindElement(SaveButton));
            element.Click();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    public string GetNickname()
    {
        Thread.Sleep(3000);
        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

        Thread.Sleep(1000);
        var element = wait.Until(d => d.FindElement(AllSettingsButton));
        element.Click();

        element = wait.Until(d => d.FindElement(AccountAndPassword));
        element.Click();
        Thread.Sleep(1000);

        element = wait.Until(d => d.FindElement(Nickname));

        return element.Text;
    }
}
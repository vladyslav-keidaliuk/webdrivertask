﻿using System.Text;

namespace UtilityLibrary;

public class TextGenerator
{

    public static string GenerateRandomText(int length)
    {
        string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < length; i++)
        {
            int index = random.Next(characters.Length);
            sb.Append(characters[index]);
        }

        return sb.ToString();
    }
}
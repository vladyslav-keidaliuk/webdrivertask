using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using UtilityLibrary;

namespace WebDriverTaskTests;

public class ChangeNicknameTests
{
    private IWebDriver driver;
    private MainPage mainPage;
    private InboxPage inboxPage;


    [SetUp]
    public void Setup()
    {
        driver = new ChromeDriver();
        driver.Manage().Window.Maximize();

        mainPage = new MainPage(driver);
        inboxPage = new InboxPage(driver);

    }

    [Test]
    public void NicknameWasSuccessfullyChanged()
    {
        driver.Url = "https://proton.me/";

        mainPage.LogIn(PersonalData.UsernameMail1, PersonalData.PasswordFromMail1);

        string newNickname = TextGenerator.GenerateRandomText(10);

        inboxPage.ChangeNickname(newNickname);

        inboxPage.LogOutFromSettings();

        driver.Url = "https://proton.me/";
        mainPage.LogIn(PersonalData.UsernameMail1, PersonalData.PasswordFromMail1);

        string result = inboxPage.GetNickname();

        Assert.AreEqual(newNickname, result);
        driver.Quit();
    }
}
﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using UtilityLibrary;

namespace WebDriverTaskTests
{
    public class SendEmailTests
    {
        private IWebDriver driver;
        private MainPage mainPage;
        private InboxPage inboxPage;
        private readonly string text = TextGenerator.GenerateRandomText(10);

        private string actualSender;
        private string actualText;
        private bool wasReplied;


        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();

            mainPage = new MainPage(driver);
            inboxPage = new InboxPage(driver);
        }

        [Test, Order(1)]
        public void EmailWasSent()
        {
            driver.Url = "https://proton.me/";
            mainPage.LogIn(PersonalData.UsernameMail1, PersonalData.PasswordFromMail1);
            bool wasSent = inboxPage.SendEmail(PersonalData.UsernameMail2, "WebDriverTestingTheme", text);

            inboxPage.LogOut();

            Assert.IsTrue(wasSent);

            driver.Quit();
        }

        [Test, Order(2)]
        public void EmailReceivedSuccessfully()
        {
            driver.Url = "https://proton.me/";
            mainPage.LogIn(PersonalData.UsernameMail2, PersonalData.PasswordFromMail2);


            bool emailExists = inboxPage.OpenUnreadMessage();

            actualSender = inboxPage.GetSender();

            actualText = inboxPage.GetText();

            wasReplied = inboxPage.SendReply();

            Assert.IsTrue(emailExists);

            driver.Quit();
        }

        [Test, Order(3)]
        public void SenderIsCorrect()
        {
            string cleanedString = actualSender.Replace("<", "").Replace(">", "");
            Assert.AreEqual(PersonalData.UsernameMail1, cleanedString);
            driver.Quit();
        }


        [Test, Order(4)]
        public void MessageIsCorrect()
        {
            Assert.AreEqual(text, actualText);
            driver.Quit();
        }


        [Test, Order(5)]
        public void ReplySuccessfuly()
        {
            Assert.IsTrue(wasReplied);

            driver.Quit();
        }
    }
}
﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using UtilityLibrary;

namespace WebDriverTaskTests
{
    public class Tests
    {
        private IWebDriver driver;
        private MainPage mainPage;
        private InboxPage inboxPage;


        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();

            mainPage = new MainPage(driver);
            inboxPage = new InboxPage(driver);
        }

        [Test]
        public void InputCorrectLoginAndPassword()
        {
            driver.Url = "https://proton.me/";

            mainPage.LogIn(PersonalData.UsernameMail1, PersonalData.PasswordFromMail1);

            string result = inboxPage.GetEmailAdress();

            Assert.AreEqual(PersonalData.UsernameMail1, result);
            driver.Quit();
        }

        [Test]
        public void InputCorrectLoginAndWrongPassword()
        {
            driver.Url = "https://proton.me/";

            mainPage.LogIn(PersonalData.UsernameMail1, PersonalData.PasswordFromMail2);

            string result = inboxPage.GetEmailAdress();

            Assert.AreEqual("None", result);
            driver.Quit();
        }

        [Test]
        public void InputWrongLoginAndWrongPassword()
        {
            driver.Url = "https://proton.me/";

            mainPage.LogIn("WebDriverOi6707Mail_1@proton.me", PersonalData.PasswordFromMail2);

            string result = inboxPage.GetEmailAdress();

            Assert.AreEqual("None", result);
            driver.Quit();
        }
    }
}